using UnityEngine;
using VG.Utilites;
using Zenject;

namespace SpaceInvaders
{
    [RequireComponent(typeof(Collider2D))]
    public sealed class Bullet : Entity, IStart,
        IFixedUpdate, ICollisionEnter2D
    {
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private Rigidbody2D _rigidbody2D;
        
        private LevelBounds _levelBounds;
        private BulletPool _pool;

        private int _damage;

        [Inject]
        public void Construct(LevelBounds levelBounds, BulletPool pool)
        {
            _levelBounds = levelBounds;
            _pool = pool;
        }
        
        void IStart.OnStart()
        {
            Add(new RigidbodyStateController(_rigidbody2D));
        }
        void ICollisionEnter2D.OnEntityCollisionEnter2D(Collision2D other)
        {
            DealDamage(other.gameObject);
            _pool.Put(this);
        }
        void IFixedUpdate.OnEntityFixedUpdate()
        {
            if(!_levelBounds.InBounds(transform.position))
                _pool.Put(this);
        }

        public void SetBullet(Args args)
        {
            transform.position = args.Position;
            _rigidbody2D.velocity = args.Velocity;
            gameObject.layer = args.PhysicsLayer;
            _spriteRenderer.color = args.Color;
            _damage = args.Damage;
        }
        
        private void DealDamage(GameObject other)
        {
            if (!other.TryGetComponent(out Unit unit))
                return;
            
            unit.Get<HitPointsComponent>().TakeDamage(_damage);
        }
    }
}