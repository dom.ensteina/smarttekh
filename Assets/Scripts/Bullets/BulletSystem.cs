using UnityEngine;

namespace SpaceInvaders
{
    public sealed class BulletSystem
    {
        private readonly BulletPool _pool;
        private readonly Transform _disable;

        public BulletSystem(BulletPool pool)
        {
            _pool = pool;
        }
        
        public void FlyBulletByArgs(Args args)
        {
            _pool.Get().SetBullet(args);
        }
    }
}