using UnityEngine;

namespace SpaceInvaders
{
    public struct Args
    {
        public readonly Vector2 Position;
        public readonly Vector2 Velocity;
        public readonly Color Color;
        public readonly int PhysicsLayer;
        public readonly int Damage;

        public Args(Vector2 position, Vector2 velocity, Color color, PhysicsLayer physicsLayer, int damage)
        {
            Position = position;
            Velocity = velocity;
            Color = color;
            PhysicsLayer = (int)physicsLayer;
            Damage = damage;
        }
    }
}