using System;
using UnityEngine;

namespace SpaceInvaders
{
    [Serializable]
    public sealed class BulletSettings
    {
        public PhysicsLayer PhysicsLayer;
        public Color Color;
        public int Damage;
        public float Speed;
    }
}