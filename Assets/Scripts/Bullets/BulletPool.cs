using System;
using Extensions;
using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public sealed class BulletPool : GameObjectsPool<Bullet>
    {
        private readonly DiContainer _container;
        
        public BulletPool(DiContainer container, Transform parent) :
            base(container.Resolve<Settings>().BulletPrefab, parent)
        {
            _container = container;
        }
        protected override Bullet Create()
        {
            var bullet = base.Create();
            _container.Inject(bullet);
            return bullet;
        }
        
        [Serializable]
        public class Settings
        {
            public Bullet BulletPrefab;
        }
    }
}