using System.Collections.Generic;

namespace SpaceInvaders
{
    public interface IGameListenerProvider
    {
        IEnumerable<IGameListener> ProvideListeners();
    }
}