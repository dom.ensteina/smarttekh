using System;
using UnityEngine;
using VG.Utilites;

namespace SpaceInvaders
{
    public enum GameState
    {
        OFF = 0,
        PLAYING = 1,
        PAUSED = 2,
        FINISHED = 3
    }
    
    public sealed class GameManager : MonoBehaviour
    {
        private GameState _state;

        public GameState State => _state;

        private void Awake()
        {
            ListenersManager.IsUpdatesEnabled = false;
        }

        public void SetState(GameState state)
        {
            if (_state == state)
                return;

            switch (state)
            {
                case GameState.PLAYING:
                    ListenersManager.IsUpdatesEnabled = true;
                    if (_state == GameState.PAUSED)
                        ListenersManager.Invoke<IGameResumeListener>(listener => listener.OnResumeGame());
                    else
                        ListenersManager.Invoke<IGameStartListener>(listener => listener.OnStartGame());
                    break;
                case GameState.PAUSED:
                    ListenersManager.IsUpdatesEnabled = false;
                    ListenersManager.Invoke<IGamePauseListener>(listener => listener.OnPauseGame());
                    break;
                case GameState.FINISHED:
                    ListenersManager.IsUpdatesEnabled = false;
                    ListenersManager.Invoke<IGameFinishListener>(listener => listener.OnFinishGame());
                    break;
            }

            _state = state;
        }
    }
}