using UniRx;
using VG.Utilites;

namespace SpaceInvaders
{
    public sealed class HitPointsComponent : EntityComponent<Unit>
    {
        private int _hitPoints;
        private readonly int _cacheHitPoint;

        public bool IsAlive => _hitPoints > 0;
        
        public ReactiveCommand<Unit> OnDeath { get; } = new ();
        
        public HitPointsComponent(int hitPoints)
        {
            _cacheHitPoint = _hitPoints = hitPoints;
        }

        public void TakeDamage(int damage)
        {
            if(!IsAlive)
                return;
            
            _hitPoints -= damage;
            if (_hitPoints <= 0)
            {
                OnDeath.Execute(Entity);
                _hitPoints = _cacheHitPoint;
            }
        }
    }
}