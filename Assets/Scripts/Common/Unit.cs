using System;
using UnityEngine;
using VG.Utilites;

namespace SpaceInvaders
{
    public abstract class Unit : Entity
    {
        [SerializeField] private Transform _firePoint;
        [SerializeField] private Rigidbody2D _rigidbody2D;
        
        protected void Construct(int hitPoint, float speed)
        {
            Add(new HitPointsComponent(hitPoint));
            Add(new WeaponComponent(_firePoint));
            Add(new MoveComponent(_rigidbody2D, speed));
        }

        [Serializable]
        public abstract class UnitSettings
        {
            public int HitPoint;
            public float Speed = 5.0f;
        }
    }
}