using System;
using UniRx;
using UnityEngine;

namespace SpaceInvaders
{
    public interface IMoveInput
    {
        public ReactiveCommand<Vector2> OnMoved { get; }
    }
}