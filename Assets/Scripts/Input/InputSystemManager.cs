using System;
using UniRx;
using UnityEngine;
using VG.Utilites;

namespace SpaceInvaders
{
    public sealed class InputSystemManager : IMoveInput, IFireInput, IUpdate,
        IGameStartListener, IGameFinishListener,
        IGamePauseListener, IGameResumeListener
    {
        public ReactiveCommand OnFired { get; } = new ();
        public ReactiveCommand<Vector2> OnMoved { get; } = new ();

        private Controls _controls;

        public InputSystemManager()
        {
            ListenersManager.Add(this);
        }

        public void OnStartGame()
        {
            _controls = new Controls();
            
            _controls.Enable();
        }
        public void OnFinishGame()
        {
            _controls.Disable();
            ListenersManager.Remove(this);
        }
        public void OnPauseGame()
        {
            _controls.Disable();
        }
        public void OnResumeGame()
        {
            _controls.Enable();
        }
        void IUpdate.OnEntityUpdate()
        {
            var moveInput = _controls.Main.Move.ReadValue<float>();
            OnMoved.Execute(new Vector2(moveInput, 0) * Time.deltaTime);
            
            if (_controls.Main.Fire.inProgress)
                OnFired.Execute();
        }
            
    }
}