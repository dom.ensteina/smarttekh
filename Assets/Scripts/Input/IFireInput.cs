using UniRx;

namespace SpaceInvaders
{
    public interface IFireInput
    {
        public ReactiveCommand OnFired { get; }
    }
}