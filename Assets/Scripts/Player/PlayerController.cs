using System;
using UnityEngine;
using VG.Utilites;
using UniRx;

namespace SpaceInvaders
{
    public sealed class PlayerController : IGameStartListener, IGameFinishListener
    {
        private const float ATTACK_DELAY = 0.2f;
        
        private readonly Player _player;
        private readonly IMoveInput _moveInput;
        private readonly IFireInput _fireInput;
        private readonly BulletSystem _bulletSystem;
        private readonly PlayerBulletSettingsProvider _settings;
        private readonly CompositeDisposable _disposables = new();
        private float _lastAttackTime;
        
        public PlayerController(Player player, IMoveInput moveInput, IFireInput fireInput, 
            BulletSystem bulletSystem, PlayerBulletSettingsProvider settings)
        {
            _player = player;
            _moveInput = moveInput;
            _fireInput = fireInput;
            _bulletSystem = bulletSystem;
            _settings = settings;
            
            ListenersManager.Add(this);
        }

        void IGameStartListener.OnStartGame()
        {
            _moveInput.OnMoved.Subscribe(_player.Get<MoveComponent>().MoveByRigidbodyVelocity).AddTo(_disposables);
            _fireInput.OnFired.Subscribe(OnFired).AddTo(_disposables);
        }
        void IGameFinishListener.OnFinishGame()
        {
            _disposables.Dispose();
            ListenersManager.Remove(this);
        }
        private void OnFired(UniRx.Unit unit)
        {
            if(ATTACK_DELAY > Time.time - _lastAttackTime)
                return;
            
            _lastAttackTime = Time.time;
            
            var weaponComponent = _player.Get<WeaponComponent>();
            var bulletSettings = _settings.CurrentSettings;
            _bulletSystem.FlyBulletByArgs(new Args
                (weaponComponent.Position, Vector3.up * _settings.CurrentSettings.Speed,
                    bulletSettings.Color, bulletSettings.PhysicsLayer, bulletSettings.Damage));
        }
        
        
        [Serializable]
        public sealed class Settings
        {
            public BulletSettings[] BulletSettings;
        }
    }
}