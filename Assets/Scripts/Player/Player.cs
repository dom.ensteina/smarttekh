using System;
using Zenject;

namespace SpaceInvaders
{
    public sealed class Player : Unit
    {
        [Inject]
        public void Construct(PlayerSettings playerSettings)
        {
            Construct(playerSettings.HitPoint, playerSettings.Speed);
        }

        [Serializable]
        public class PlayerSettings : UnitSettings
        {
        }
    }
}