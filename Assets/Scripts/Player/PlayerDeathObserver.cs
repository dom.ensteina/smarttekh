﻿using System;
using UniRx;
using VG.Utilites;

namespace SpaceInvaders
{
    public class PlayerDeathObserver : IGameFinishListener
    {
        private readonly GameManager _gameManager;
        private readonly Player _player;
        private readonly IDisposable _disposable;

        public PlayerDeathObserver(GameManager gameManager, Player player)
        {
            _gameManager = gameManager;
            _player = player;
            
            _disposable = _player.Get<HitPointsComponent>().OnDeath.Subscribe(OnDeath);
            ListenersManager.Add(this);
        }
        
        public void OnFinishGame()
        {
            _disposable.Dispose();
            ListenersManager.Remove(this);
        }
        private void OnDeath(Unit unit)
        {
            _gameManager.SetState(GameState.FINISHED);
        }
    }
}