using UnityEngine;

namespace SpaceInvaders
{
    public sealed class PlayerBulletSettingsProvider
    {
        private readonly BulletSettings[] _bulletSettings;
        private int _currentSettingsIndex = 0;
        
        public BulletSettings CurrentSettings => _bulletSettings[_currentSettingsIndex];
        
        public PlayerBulletSettingsProvider(PlayerController.Settings settings)
        {
            _bulletSettings = settings.BulletSettings;
        }

        public bool TryGetNextSettings(out Color color)
        {
            color = default;
            if (_currentSettingsIndex + 1 >= _bulletSettings.Length)
                return false;
            
            color = _bulletSettings[_currentSettingsIndex + 1].Color;
            return true;
        }
        public void UpgradeSettings()
        {
            _currentSettingsIndex++;
        }
    }
}