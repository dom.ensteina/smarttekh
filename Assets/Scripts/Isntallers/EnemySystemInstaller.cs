using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public class EnemySystemInstaller : MonoInstaller
    {
        
        [SerializeField] private Transform _enemiesPoolContainer;
        [SerializeField] private Transform[] _spawnPositions;
        [SerializeField] private Transform[] _attackPositions;
        
        public override void InstallBindings()
        {
            Container.Bind<EnemyPool>().AsSingle().WithArguments(_enemiesPoolContainer);
            Container.Bind<EnemyManager>().AsSingle();
            Container.Bind<EnemyPeriodSpawner>().AsSingle().NonLazy();
            Container.Bind<EnemyPositions>().AsSingle().WithArguments(_spawnPositions, _attackPositions);
            Container.BindInterfacesAndSelfTo<SpawnEnemyCounter>().AsSingle();
        }
    }
}