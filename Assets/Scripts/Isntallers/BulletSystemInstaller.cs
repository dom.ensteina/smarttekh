using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public class BulletSystemInstaller : MonoInstaller
    {
        [SerializeField] private Transform _bulletsPoolContainer;
        
        public override void InstallBindings()
        {
            Container.Bind<BulletPool>().AsSingle().WithArguments(_bulletsPoolContainer);
            Container.Bind<BulletSystem>().AsSingle();
        }
    }
}