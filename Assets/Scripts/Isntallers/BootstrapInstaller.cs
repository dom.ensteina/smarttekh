using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public class BootstrapInstaller : MonoInstaller
    {
        [SerializeField] private Player _player;
        [SerializeField] private BulletUpgradeView _bulletUpgradeView;
        [SerializeField, Range(0f, 1f)] private float _bulletUpgradePercent = 0.5f;
        [SerializeField] private GameManager _gameManager;
        [SerializeField] private Transform _levelBackground;
        [SerializeField] private LevelBackground.Params _levelBackgroundParameters;
        [SerializeField] private Transform[] _levelBounds;
        [SerializeField] private int _countEnemiesPerRound = 20;

        public override void InstallBindings()
        {
            Container.Bind<Player>().FromInstance(_player).AsSingle();
            Container.Bind<PlayerController>().AsSingle().NonLazy();
            Container.Bind<PlayerDeathObserver>().AsSingle().NonLazy();
            Container.Bind<PlayerBulletSettingsProvider>().AsSingle();
            Container.BindInterfacesAndSelfTo<BulletUpgradeSpawner>().AsSingle().
                WithArguments(_bulletUpgradeView, _bulletUpgradePercent);
            
            Container.Bind<GameManager>().FromInstance(_gameManager).AsSingle();
            Container.Bind<LevelBackground>().AsSingle().
                WithArguments(_levelBackground, _levelBackgroundParameters).NonLazy();
            Container.Bind<LevelBounds>().AsSingle().WithArguments(_levelBounds);
            Container.BindInterfacesTo<RoundSystem>().AsSingle().WithArguments(_countEnemiesPerRound).NonLazy();
            
            Container.Bind(typeof(IFireInput), typeof(IMoveInput)).To<InputSystemManager>().AsSingle();
        }
    }
}
