using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    //[CreateAssetMenu(menuName = "Game Settings")]
    public class GameSettingsInstaller : ScriptableObjectInstaller
    {
        [SerializeField] private EnemyManager.Settings _enemyManager;
        [SerializeField] private PlayerController.Settings _playerController;
        [SerializeField] private Player.PlayerSettings _playerSettings;
        [SerializeField] private Enemy.EnemySettings _enemySettings;
        [SerializeField] private EnemyPool.Settings _enemyPool;
        [SerializeField] private BulletPool.Settings _bulletPool;
        
        public override void InstallBindings()
        {
            Container.BindInstance(_enemyManager);
            Container.BindInstance(_playerController);
            Container.BindInstance(_playerSettings);
            Container.BindInstance(_enemySettings);
            Container.BindInstance(_enemyPool);
            Container.BindInstance(_bulletPool);
        }
    }
}