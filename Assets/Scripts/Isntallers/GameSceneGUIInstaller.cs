using UI;
using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public class GameSceneGUIInstaller : MonoInstaller
    {
        [SerializeField] private uint _timeToStart = 3;
        [SerializeField] private StartGameView _startGameView;
        [SerializeField] private PauseButtonView _pauseButtonView;
        [SerializeField] private PauseMenuView _pauseMenuView;
        [SerializeField] private FinishGameView _finishGameView;
        [SerializeField] private KillsCounterView _killsCounterView;
        [SerializeField] private JoystickView _joystickView;
        [SerializeField] private RoundView _roundView;

        public override void InstallBindings()
        {
            Container.Bind<StartGameController>().AsSingle().WithArguments(_startGameView, _timeToStart).NonLazy();
            
            Container.BindInterfacesTo<PauseButtonController>().AsSingle().WithArguments(_pauseButtonView).NonLazy();

            Container.BindInterfacesTo<PauseMenuController>().AsSingle().WithArguments(_pauseMenuView).NonLazy();

            Container.BindInterfacesTo<FinishGameController>().AsSingle().WithArguments(_finishGameView).NonLazy();
            
            Container.Bind<KillsCounterModel>().AsSingle();
            Container.BindInterfacesAndSelfTo<KillsCounterController>().AsSingle().WithArguments(_killsCounterView).NonLazy();
            
            Container.BindInterfacesTo<JoystickController>().AsSingle().WithArguments(_joystickView).NonLazy();
            
            Container.Bind<RoundController>().AsSingle().WithArguments(_roundView).NonLazy();
        }
    }
}