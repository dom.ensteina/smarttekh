using UnityEngine;
using VG.Utilites;

namespace SpaceInvaders
{
    public sealed class EnemyPeriodSpawner : IUpdate
    {
        private readonly EnemyManager _enemyManager;
        private readonly float _delayBetweenSpawnsTime = 1f;
        private float _lastSpawnTime;
        
        public EnemyPeriodSpawner(EnemyManager enemyManager)
        {
            _enemyManager = enemyManager;
        }

        void IUpdate.OnEntityUpdate()
        {
            if (Time.realtimeSinceStartup - _lastSpawnTime <= _delayBetweenSpawnsTime) 
                return;
            
            if(_enemyManager.TrySpawnEnemy())
                _lastSpawnTime = Time.realtimeSinceStartup;
        }
    }
}