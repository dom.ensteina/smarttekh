using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace SpaceInvaders
{
    public sealed class EnemyManager
    {
        private readonly EnemyPool _pool;
        private readonly BulletSystem _bulletSystem;
        private readonly Settings _settings;
        private readonly Player _player;
        private readonly EnemyPositions _enemyPositions;
        private readonly DictionaryDisposable<Enemy, CompositeDisposable> _disposables = new ();

        public EnemyManager(EnemyPool pool, BulletSystem bulletSystem, Settings settings, Player player, 
            EnemyPositions enemyPositions)
        {
            _pool = pool;
            _bulletSystem = bulletSystem;
            _settings = settings;
            _player = player;
            _enemyPositions = enemyPositions;
        }
        
        public bool TrySpawnEnemy()
        {
            if(!_pool.TryGet(out var enemy))
                return false;
            
            enemy.transform.position = _enemyPositions.GetRandomSpawnPosition();
                    
            var attackPosition = _enemyPositions.GetRandomAttackTransformPosition(enemy);
            enemy.Get<EnemyMoveAgent>().SetDestination(attackPosition.position);

            enemy.Get<EnemyAttackAgent>().SetTarget(_player);

            var disposables = new CompositeDisposable();
            _disposables.Add(enemy, disposables);
            enemy.Get<HitPointsComponent>().OnDeath.Subscribe(OnDeath).AddTo(disposables);
            enemy.Get<EnemyAttackAgent>().OnFired += OnFired;

            return true;
        }
        
        private void OnDeath(Unit unit)
        {
            if (unit is Enemy enemy)
            {
                if(_disposables.Remove(enemy, out var disposable))
                    disposable.Dispose();

                _pool.Put(enemy);
                _enemyPositions.PutAttackTransform(enemy);
            }
        }
        private void OnFired(AttackConfig attackConfig)
        {
            var bulletSettings = _settings.BulletSettings;
            _bulletSystem.FlyBulletByArgs(new Args(attackConfig.Position, attackConfig.Direction * bulletSettings.Speed, 
                bulletSettings.Color, bulletSettings.PhysicsLayer, bulletSettings.Damage));
        }
        
        [Serializable]
        public sealed class Settings
        {
             public BulletSettings BulletSettings;
        }
    }
}