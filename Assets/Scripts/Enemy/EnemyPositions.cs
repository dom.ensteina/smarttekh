using System.Collections.Generic;
using UnityEngine;

namespace SpaceInvaders
{
    public sealed class EnemyPositions
    {
        private readonly Transform[] _spawnPositions; 
        private readonly List<Transform> _attackPositions;
        private readonly Dictionary<Enemy, Transform> _busyTransforms = new();

        public EnemyPositions(Transform[] spawnPositions, Transform[] attackPositions)
        {
            _spawnPositions = spawnPositions;
            _attackPositions = new List<Transform>(attackPositions);
        }
        
        public Vector3 GetRandomSpawnPosition()
        {
            var index = Random.Range(0, _spawnPositions.Length);
            return _spawnPositions[index].position;
        }
        public Transform GetRandomAttackTransformPosition(Enemy enemy)
        {
            var index = Random.Range(0, _attackPositions.Count);
            var trans = _attackPositions[index];
            _attackPositions.RemoveAt(index);
            _busyTransforms.Add(enemy, trans);
            return trans;
        }
        public void PutAttackTransform(Enemy enemy)
        {
            var trans = _busyTransforms[enemy];
            _busyTransforms.Remove(enemy);
            _attackPositions.Add(trans);
        }
    }
}