﻿using System;
using UniRx;

namespace SpaceInvaders
{
    public sealed class SpawnEnemyCounter : IDisposable
    {
        private readonly EnemyPool _pool;
        private readonly IntReactiveProperty _spawnEnemyCount = new();
        private readonly IDisposable _disposable;
        
        public IReadOnlyReactiveProperty<int> SpawnEnemyCount => _spawnEnemyCount;

        public SpawnEnemyCounter(EnemyPool pool)
        {
            _pool = pool;
            
            _disposable = _pool.OnEnemyGet.Subscribe(OnEnemySpawned);
        }

        void IDisposable.Dispose()
        {
            _disposable.Dispose();
        }
        
        private void OnEnemySpawned(Enemy enemy)
        {
            _spawnEnemyCount.Value++;
        }
    }
}