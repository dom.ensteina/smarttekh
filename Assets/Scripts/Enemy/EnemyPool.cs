using System;
using Extensions;
using UniRx;
using UnityEngine;
using Zenject;

namespace SpaceInvaders
{
    public sealed class EnemyPool : GameObjectsPool<Enemy>
    {
        private readonly GameManager _gameManager;
        private readonly DiContainer _diContainer;
        
        public ReactiveCommand<Enemy> OnEnemyGet { get; } = new();
        public ReactiveCommand<Enemy> OnEnemyPut { get; } = new();
        
        public EnemyPool(DiContainer diContainer, Settings settings, Transform container) : 
            base(settings.EnemyPrefab, container, settings.MaxCount)
        {
            _diContainer = diContainer;
        }
        protected override Enemy Create()
        {
            var obj = base.Create();
            
            _diContainer.Inject(obj);
            return obj;
        }
        public override bool TryGet(out Enemy enemy)
        {
            enemy = base.Get();
            OnEnemyGet.Execute(enemy);
            return enemy;
        }
        public override Enemy Get()
        {
            var enemy = base.Get();
            OnEnemyGet.Execute(enemy);
            return enemy;
        }
        public override void Put(Enemy enemy)
        {
            OnEnemyPut.Execute(enemy);
            base.Put(enemy);
        }

        [Serializable]
        public class Settings
        {
            public int MaxCount = 10;
            public Enemy EnemyPrefab;
        }
    }
}