using System;
using Zenject;

namespace SpaceInvaders
{
    public sealed class Enemy : Unit
    {
        [Inject]
        public void Construct(EnemySettings enemySettings)
        {
            base.Construct(enemySettings.HitPoint, enemySettings.Speed);
            
            Add(new EnemyMoveAgent(enemySettings.PositionInaccuracy));
            Add(new EnemyAttackAgent(enemySettings.ShootDelay));
        }
        
        [Serializable]
        public class EnemySettings : UnitSettings
        {
            public float PositionInaccuracy = 0.25f;
            public float ShootDelay = 1;
        }
    }
}