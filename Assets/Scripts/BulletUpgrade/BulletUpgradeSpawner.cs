using System;
using Random = UnityEngine.Random;
using UniRx;

namespace SpaceInvaders
{
    public sealed class BulletUpgradeSpawner : IDisposable
    {
        private readonly PlayerBulletSettingsProvider _provider;
        private readonly BulletUpgradeView _settingsView;
        private readonly EnemyPool _enemyPool;
        private readonly float _percent;
        private readonly CompositeDisposable _disposable = new ();

        public BulletUpgradeSpawner(PlayerBulletSettingsProvider provider, BulletUpgradeView settingsView, 
            EnemyPool enemyPool, float percent)
        {
            _provider = provider;
            _settingsView = settingsView;
            _enemyPool = enemyPool;
            _percent = percent;


            _settingsView.OnTrigger.Subscribe(_ => _provider.UpgradeSettings()).AddTo(_disposable);
            _enemyPool.OnEnemyPut.Subscribe(TrySpawn).AddTo(_disposable);
        }
    
        void IDisposable.Dispose()
        {
            _disposable.Dispose();
        }
        
        private void TrySpawn(Enemy enemy)
        {
            if (_provider.TryGetNextSettings(out var color) && _percent > Random.value)
                _settingsView.TrySpawnUpgradeSettings(color, enemy.transform.position);
        }
    }
}