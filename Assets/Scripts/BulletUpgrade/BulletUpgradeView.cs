using System;
using UniRx;
using UnityEngine;

namespace SpaceInvaders
{
    public sealed class BulletUpgradeView : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _sprite;
        private bool _isActive = true;

        public ReactiveCommand OnTrigger { get; } = new ();
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            OnTrigger.Execute();
            gameObject.SetActive(false);
            _isActive = true;
        }

        public bool TrySpawnUpgradeSettings(Color color, Vector2 position)
        {
            if(!_isActive)
                return false;
            
            _sprite.color = color;
            transform.position = position;
            gameObject.SetActive(true);
            _isActive = false;
            return true;
        }
    }
}