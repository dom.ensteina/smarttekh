﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public sealed class RoundView : BaseView
    {
        [SerializeField] private Text _roundText;

        public void Show(string round)
        {
            _roundText.text = $"Round: {round}";
            base.Show();
        }
    }
}