namespace UI
{
    public sealed class RoundController
    {
        private readonly RoundView _view;

        public RoundController(RoundView view)
        {
            _view = view;
        }
        
        public void Show(int round)
        {
            _view.Show(round.ToString());
        }
        public void Hide()
        {
            _view.Hide();
        }
    }
}