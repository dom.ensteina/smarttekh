﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public sealed class KillsCounterView : BaseView
    {
        private const float SCALE = 1.2f;
        private const float DURATION = 0.25f;
        
        [SerializeField] private Text _killsCounterText;
        private Tweener _tween;
        
        public void SetKills(string kills)
        {
            _tween?.Kill(true);
            
            _tween = _killsCounterText.transform.DOScale(Vector3.one * SCALE, DURATION).SetLoops(2, LoopType.Yoyo)
                .SetEase(Ease.Linear).OnStepComplete(() => _killsCounterText.text = kills);
        }
    }
}