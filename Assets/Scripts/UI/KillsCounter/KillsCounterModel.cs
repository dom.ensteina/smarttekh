﻿using UniRx;

namespace UI
{
    public sealed class KillsCounterModel
    {
        private readonly ReactiveProperty<int> _kills = new ();
        
        public IReadOnlyReactiveProperty<int> Kills => _kills;

        public void AddKill()
        {
            _kills.Value++;
        }
    }
}