﻿using System;
using SpaceInvaders;
using UniRx;
using VG.Utilites;

namespace UI
{
    public sealed class KillsCounterController : IDisposable, IGameStartListener
    {
        private readonly KillsCounterModel _model;
        private readonly KillsCounterView _view;
        private readonly EnemyPool _enemyPool;
        private readonly CompositeDisposable _disposables = new();
        
        public KillsCounterController(KillsCounterModel model, KillsCounterView view, EnemyPool enemyPool)
        {
            _model = model;
            _view = view;
            _enemyPool = enemyPool;
            _enemyPool.OnEnemyPut.Subscribe(AddKill).AddTo(_disposables);

            _model.Kills.Subscribe(k => _view.SetKills(k.ToString())).AddTo(_disposables);
            ListenersManager.Add(this);
        }

        void IGameStartListener.OnStartGame()
        {
            _view.Show();
        }
        void IDisposable.Dispose()
        {
            _disposables.Dispose();
            ListenersManager.Remove(this);
        }
        
        private void AddKill(Enemy enemy)
        {
            _model.AddKill();
        }
    }
}