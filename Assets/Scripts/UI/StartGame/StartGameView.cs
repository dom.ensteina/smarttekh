using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public sealed class StartGameView : BaseView
    {
        [SerializeField] private Button _startButton;
        [SerializeField] private Text _text;
        
        private IObservable<Button> _onStartGame;
        
        public IObservable<Button> OnStartGame => _onStartGame ??= 
            _startButton.OnClickAsObservable().Select(_ => _startButton);

        private void OnEnable()
        {
            OnStartGame.Subscribe(StartGame).AddTo(this);
        }

        public override void Hide()
        {
            base.Hide();
            _startButton.onClick.RemoveAllListeners();
        }
        public void SetTimerText(string text)
        {
            _text.text = text;
        }

        private void StartGame(Button button)
        {
            _text.gameObject.SetActive(true);
            _startButton.gameObject.SetActive(false);
        }
    }
}