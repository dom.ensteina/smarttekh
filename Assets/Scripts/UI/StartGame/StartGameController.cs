using System;
using SpaceInvaders;
using UniRx;
using UnityEngine;

namespace UI
{
    public sealed class StartGameController
    {
        private const uint DELAY_SECONDS = 1;
        
        private readonly StartGameView _view;
        private readonly GameManager _gameManager;
        private readonly uint _timeToStart;
        private CompositeDisposable _disposables = new ();

        public StartGameController(StartGameView view, GameManager gameManager, uint timeToStart)
        {
            _view = view;
            _gameManager = gameManager;
            _timeToStart = timeToStart;
            
            _view.OnStartGame.Subscribe(_ => StartGame()).AddTo(_disposables);
        }
        
        private void StartGame()
        {
            SetTimerText(_timeToStart);
            Observable.Interval(TimeSpan.FromSeconds(DELAY_SECONDS)).
                Subscribe(time => SetTimerText(_timeToStart - time - 1)).AddTo(_disposables);
        }
        private void SetTimerText(long time)
        {
            _view.SetTimerText(time.ToString());
            
            if (time == 0)
                StopTimer();
        }
        private void StopTimer()
        {
            _disposables.Dispose();
            _gameManager.SetState(GameState.PLAYING);
            _view.Hide();
        }
    }
}