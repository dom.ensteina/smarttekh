using System;
using SpaceInvaders;
using UnityEngine.SceneManagement;
using VG.Utilites;
using UniRx;

namespace UI
{
    public sealed class PauseMenuController : IDisposable,
        IGamePauseListener, IGameResumeListener
    {
        private readonly PauseMenuView _view;
        private readonly GameManager _gameManager;
        private readonly CompositeDisposable _disposables = new ();
        
        public PauseMenuController(PauseMenuView view, GameManager gameManager)
        {
            _view = view;
            _gameManager = gameManager;
            
            _view.OnResume.Subscribe(_ => OnResume()).AddTo(_disposables);
            _view.OnRestart.Subscribe(_ => OnRestart()).AddTo(_disposables);
            
            ListenersManager.Add(this);
        }

        void IDisposable.Dispose()
        {
            _disposables?.Dispose();
            ListenersManager.Remove(this);
        }
        void IGamePauseListener.OnPauseGame()
        {
            _view.Show();
        }
        void IGameResumeListener.OnResumeGame()
        {
            _view.Hide();
        }
        
        private void OnRestart()
        {
            ListenersManager.Clear();
            SceneManager.LoadScene(SceneManager.GetActiveScene().path);
        }
        private void OnResume()
        {
            _gameManager.SetState(GameState.PLAYING);
        }
    }
}