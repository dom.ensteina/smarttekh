﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public sealed class PauseMenuView : BaseView
    {
        [SerializeField] private Button _resumeButton;
        [SerializeField] private Button _restartButton;
        private IObservable<Button> _onResume;
        private IObservable<Button> _onRestart;
        
        private readonly CompositeDisposable _disposables = new ();

        public IObservable<Button> OnResume => _onResume ??= 
            _resumeButton.OnClickAsObservable().Select(_ => _resumeButton); 
        
        public IObservable<Button> OnRestart => _onRestart ??= 
            _restartButton.OnClickAsObservable().Select(_ => _restartButton); 
        

        private void OnDestroy()
        {
            _disposables?.Dispose();
        }
    }
}