using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public sealed class PauseButtonView : BaseView
    {
        [SerializeField] private Button _pauseButton;
        
        private IObservable<Button> _onPauseClicked;
        
        public IObservable<Button> OnPauseClicked => _onPauseClicked ??= 
            _pauseButton.OnClickAsObservable().Select(_ => _pauseButton);
    }
}