﻿using System;
using SpaceInvaders;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;
using VG.Utilites;

namespace UI
{
    public sealed class PauseButtonController : IDisposable, 
        IGameStartListener, IGameFinishListener, IGamePauseListener, IGameResumeListener 
    {
        private readonly PauseButtonView _view;
        private readonly GameManager _gameManager;
        private readonly IDisposable _disposable;

        public PauseButtonController(PauseButtonView view, GameManager gameManager)
        {
            _view = view;
            _gameManager = gameManager;
            
            _disposable = _view.OnPauseClicked.Subscribe(_ => OnPauseClicked());
            
            ListenersManager.Add(this);
        }

        void IDisposable.Dispose()
        {
            _disposable.Dispose();
            ListenersManager.Remove(this);
        }
        
        private void OnPauseClicked()
        {   
            _gameManager.SetState(GameState.PAUSED);
        }
        
        void IGameStartListener.OnStartGame()
        {
            _view.Show();
        }
        void IGameFinishListener.OnFinishGame()
        {
            _view.Hide();
        }
        void IGamePauseListener.OnPauseGame()
        {
            _view.Hide();
        }
        void IGameResumeListener.OnResumeGame()
        {
            _view.Show();
        }
    }
}