﻿using System;
using SpaceInvaders;
using VG.Utilites;

namespace UI
{
    public sealed class JoystickController : IDisposable, IGameStartListener, IGameFinishListener,
        IGamePauseListener, IGameResumeListener
    {
        private readonly JoystickView _view;

        public JoystickController(JoystickView view)
        {
            _view = view;
            ListenersManager.Add(this);
        }
        
        void IDisposable.Dispose()
        {
            ListenersManager.Remove(this);
        }
        void IGameStartListener.OnStartGame()
        {
            _view.Show();
        }
        void IGameFinishListener.OnFinishGame()
        {
            _view.Hide();
        }
        void IGamePauseListener.OnPauseGame()
        {
            _view.Hide();
        }
        void IGameResumeListener.OnResumeGame()
        {
            _view.Show();
        }
    }
}