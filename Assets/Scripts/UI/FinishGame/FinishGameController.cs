using System;
using SpaceInvaders;
using VG.Utilites;

namespace UI
{
    public sealed class FinishGameController : IDisposable, IGameFinishListener
    {
        private readonly FinishGameView _view;

        public FinishGameController(FinishGameView view)
        {
            _view = view;
            ListenersManager.Add(this);
        }

        void IDisposable.Dispose()
        {
            ListenersManager.Remove(this);
        }
        void IGameFinishListener.OnFinishGame()
        {
            _view.Show();
        }
    }
}