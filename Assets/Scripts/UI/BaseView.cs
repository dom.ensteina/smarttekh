﻿using DG.Tweening;
using UnityEngine;

namespace UI
{
    public abstract class BaseView : MonoBehaviour
    {
        private readonly float _showScale = 1;
        private readonly float _hideScale = 0;
        private readonly float _duration = 0.25f;
        
        public virtual void Show()
        {
            if(gameObject.activeSelf)
                return;

            gameObject.SetActive(true);
            transform.localScale = Vector3.zero;
            transform.DOScale(_showScale, _duration).SetEase(Ease.OutBack);
        }
        public virtual void Hide()
        {
            if(!gameObject.activeSelf)
                return;
            
            transform.DOScale(_hideScale, _duration).SetEase(Ease.OutBack).
                OnComplete(() => gameObject.SetActive(false));
        }
    }
}