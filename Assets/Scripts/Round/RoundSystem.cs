﻿using System;
using UI;
using UniRx;
using VG.Utilites;

namespace SpaceInvaders
{
    public sealed class RoundSystem : IDisposable, IGameStartListener
    {
        private const uint DELAY_SECONDS = 3;
        
        private int _currentRound;
        private readonly EnemyPeriodSpawner _enemyPeriodSpawner;
        private readonly RoundController _roundController;
        private readonly int _countEnemiesPerRound;
        private readonly CompositeDisposable _disposable = new();
        private IDisposable _intervalDisposable;

        public RoundSystem(SpawnEnemyCounter spawnEnemyCounter, EnemyPeriodSpawner enemyPeriodSpawner, 
            RoundController roundController, KillsCounterModel killsCounterModel, int countEnemiesPerRound)
        {
            _enemyPeriodSpawner = enemyPeriodSpawner;
            _roundController = roundController;
            _countEnemiesPerRound = countEnemiesPerRound;

            spawnEnemyCounter.SpawnEnemyCount.Subscribe(OnSpawnEnemy).AddTo(_disposable);
            killsCounterModel.Kills.SkipLatestValueOnSubscribe().Subscribe(OnKillsEnemy).AddTo(_disposable);
            
            ListenersManager.Add(this);
        }
        
        void IDisposable.Dispose()
        {
            _disposable.Dispose();
        }
        void IGameStartListener.OnStartGame()
        {
            OnKillsEnemy(0);
            ListenersManager.Remove(this);
        }

        private void OnSpawnEnemy(int count)
        {
            if (count % _countEnemiesPerRound != 0)
                return;
            
            ListenersManager.Remove(_enemyPeriodSpawner);
            _currentRound++;
        }
        private void OnKillsEnemy(int count)
        {
            if (count % _countEnemiesPerRound != 0)
                return;
            
            _roundController.Show(_currentRound);
            Hide();
        }
        private void Hide()
        {
            _intervalDisposable = Observable.Interval(TimeSpan.FromSeconds(DELAY_SECONDS)).
                Subscribe(time =>
                {
                    _roundController.Hide();
                    ListenersManager.Add(_enemyPeriodSpawner);
                    _intervalDisposable.Dispose();
                });
        }
    }
}
