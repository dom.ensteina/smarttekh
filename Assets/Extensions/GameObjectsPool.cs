using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Extensions
{
    public abstract class GameObjectsPool<T> where T : Component
    {
        private readonly T _prefab;
        private readonly Transform _container;
        private readonly HashSet<T> _active = new();
        private readonly Queue<T> _disabled = new();
        private readonly int _maxSize;

        public int Count => _active.Count;
        public bool HasFreeObject => _active.Count < _maxSize;

        protected GameObjectsPool(T prefab, Transform container, int maxSize = int.MaxValue)
        {
            if (prefab == null)
                throw new NullReferenceException();
            
            _prefab = prefab;

            _container = container;
            
            _maxSize = Mathf.Clamp(maxSize, 1, int.MaxValue);
        }

        public virtual bool TryGet(out T obj)
        {
            obj = default;
            if (_active.Count == _maxSize)
                return false;
            else
                obj = GetOrCreate();
            
            return true;
        }
        public virtual T Get()
        {
            T obj;

            if (_active.Count == _maxSize)
                return null;
            else
                obj = GetOrCreate();
            
            return obj;
        }
        public virtual void Put(T obj)
        {
            if(!_active.Remove(obj))
                return;
            
            _disabled.Enqueue(obj);
            obj.gameObject.SetActive(false);
        }

        private T GetOrCreate()
        {
            var obj = _disabled.Count > 0 ? _disabled.Dequeue() : Create();
            
            obj.gameObject.SetActive(true);
            _active.Add(obj);

            return obj;
        }
        protected virtual T Create()
        {
            var obj = Object.Instantiate(_prefab, _container);
            obj.transform.SetParent(_container); 
            return obj;
        }
    }
}